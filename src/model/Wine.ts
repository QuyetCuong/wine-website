export class Wine {
    id: string;
    name: string;
    price: number;
    detail: string;

    constructor(id: string, name: string, price: number, detail: string) {
      this.id = id;
      this.name = name;
      this.price = price;
      this.detail = detail;
    }
}
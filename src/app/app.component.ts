import { animate, style, transition, trigger } from '@angular/animations';
import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate('300ms 100ms ease-out', style({ opacity: 1 }))
      ]),
      transition('* => void', [
        style({ opacity: 1 }),
        animate('300ms 100ms ease-out', style({ opacity: 0 }))
      ])
    ]
    )
  ],
})
export class AppComponent {
  public innerWidth: any;
  public isMobile: boolean;
  public isOpenMenu: boolean;
  public heightContent: string;
  public widthContent: string;
  public marginLeftContent: string;

  public listItemMenuMobile = [
    {
      link: "/",
      title: "Trang chủ",
      isOpen: false,
      listSubItem: []
    },
    {
      link: "/product",
      title: "Vang cao cấp",
      isOpen: false,
      listSubItem: [
        {
          subName: 'Vang 1',
          href: '#'
        },
        {
          subName: 'Vang 2',
          href: '#'
        }
      ]
    },
    {
      link: "/product",
      title: "Rượu vang",
      isOpen: false,
      listSubItem: [
        {
          subName: 'Rượu Vang 1',
          href: '#'
        },
        {
          subName: 'Rượu Vang 2',
          href: '#'
        }
      ]
    },
    {
      link: "/product",
      title: "Bia nhập khẩu",
      isOpen: false,
      listSubItem: [
        {
          subName: 'Bia nhập khẩu 1',
          href: '#'
        },
        {
          subName: 'Bia nhập khẩu 2',
          href: '#'
        }
      ]
    },
    {
      link: "/product",
      title: "Champange",
      isOpen: false,
      listSubItem: [
        {
          subName: 'Champange 1',
          href: '#'
        },
        {
          subName: 'Champange 2',
          href: '#'
        }
      ]
    },
    {
      link: "/contact",
      title: "Liên hệ",
      isOpen: false,
      listSubItem: []
    }
  ]

  constructor() {
    this.innerWidth = window.innerWidth;
    this.isMobile = this.innerWidth < 768;
    this.isOpenMenu = false;
    this.heightContent = this.isMobile ? '100%' : '90%';
    this.widthContent = this.isMobile ? '90%' : '100%';
    this.marginLeftContent = this.isMobile ? '50px' : '0px';
  }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    this.isMobile = this.innerWidth < 768;
    this.heightContent = this.isMobile ? '100%' : '90%';
    this.widthContent = this.isMobile ? '90%' : '100%';
    this.marginLeftContent = this.isMobile ? '50px' : '0px';
  }

  onChangeMenu() {
    this.isOpenMenu = !this.isOpenMenu;
  }

  onShowSubItems(itemMenu: any) {
    itemMenu.isOpen = !itemMenu.isOpen
  }
}

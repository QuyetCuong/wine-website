import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import {
  NgbCarousel,
  NgbSlideEvent,
  NgbSlideEventSource,
} from '@ng-bootstrap/ng-bootstrap';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  providers: [NgbCarouselConfig],
})
export class HomePageComponent implements OnInit {
  images = [
    {
      path: 'https://ruoutot.net/uploads//settings/4/1615888221-anh-bia-web-1.jpg',
    },
    {
      path: 'https://ruoutot.net/uploads//settings/4/1594786758-mua-ruou-vang-tang-tu.jpg',
    },
  ];
  imageObject = [
    {
      image:
        'https://ruoutot.net/uploads//settings/4/1615888221-anh-bia-web-1.jpg',
      thumbImage:
        'https://ruoutot.net/uploads//settings/4/1615888221-anh-bia-web-1.jpg',
    },
    {
      image:
        'https://ruoutot.net/uploads//settings/4/1594786758-mua-ruou-vang-tang-tu.jpg',
      thumbImage:
        'https://ruoutot.net/uploads//settings/4/1594786758-mua-ruou-vang-tang-tu.jpg',
    },
    {
      image:
        'https://ruoutot.net/uploads//settings/4/1imageObject615888610-anh-bia-wweb-2.jpg',
      thumbImage:
        'https://ruoutot.net/uploads//settings/4/1615888610-anh-bia-wweb-2.jpg',
    },
    {
      image:
        'https://ruoutot.net/uploads//settings/4/1615888221-anh-bia-web-1.jpg',
      thumbImage:
        'https://ruoutot.net/uploads//settings/4/1615888221-anh-bia-web-1.jpg',
    },
  ];
  innerWidth: number;
  imageSize: any;

  products = [
    {
      img: 'https://ruoutot.net/uploads/products/1/4514-5-2.jpg',
      name: 'Rượu vang Opus One Napa Valley Cao cấp',
      price: '14500000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/1/38914-vang-phap-chateau-tayac-margaux.jpg',
      name: 'Vang Pháp Chateau Tayac Margaux',
      price: '998000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/1/34350-old-world-cuvee-99.jpg',
      name: 'Vang Ý Old World Cuvee 99 Nhập khẩu cao cấp thượng hạng',
      price: '14500000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/1/21236-la-bastide-de-dauzac-margaux.jpg',
      name: 'Vang Pháp LaBastide Dauzac Margaux Cao cấp',
      price: '1160000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/1/3508-7-2.jpg',
      name: 'Vang Ý 100 ESSENZA Primitivo di Manduria',
      price: '1100000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/4/1607848249-gran-appasso-appassimeto.jpg',
      name: 'Vang Ý Gran Appasso Appassimento Primitivo 16 độ',
      price: '1550000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/4/1605521877-la-curte-primitivo-di-manduria.jpg',
      name: 'Vang Ý La Curte Limited 19 độ Primitivo di Manduria',
      price: '3200000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/4/1594975400-ruou-vang-24k-gold-nebbiolo-plozza.jpg',
      name: 'Vang Ý 24K Gold Nebbiolo Plozza Thượng hạng',
      price: '24850000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/1/2881-ruou-vang-y-f.jpg',
      name: 'Rượu Vang Ý F Negroamaro Salentino chữ F đặc biệt',
      price: '1290000'
    },
    {
      img: 'https://ruoutot.net/uploads/products/1/6013-1-1.jpg',
      name: 'Vang Ý Monteverdi Dolce Novella - Vang Hoàng Đế',
      price: '285000'
    },
  ];

  constructor() {
    this.innerWidth = window.innerWidth;
    this.imageSize = this.innerWidth <= 768 ? { width: '100%', height: '30%' } : { width: '100%', height: '50%' };
  }

  ngOnInit(): void {}

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    this.imageSize = this.innerWidth <= 768 ? { width: '100%', height: '30%' } : { width: '100%', height: '50%' };
  }
}

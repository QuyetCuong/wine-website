import { ProductPageComponent } from './page/product-page/product-page.component';
import { HomePageComponent } from './page/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// const routes: Routes = [
//   { path: '', component: HomePageComponent },
//   { path: 'product', component: ProductPageComponent }
// ];

const routes: Routes = [
  { path: '', loadChildren: () => import('./page/home-page/home-page.module').then(m => m.HomePageModule) },
  { path: 'product', loadChildren: () => import('./page/product-page/product-page.module').then(m => m.ProductPageModule) }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
